import csv
from dataclasses import dataclass, field
import operator
from scipy.stats import variation
from typing import List
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

SUMLEV = '162'  # 162 or 157

with open('data\\us_cities.csv') as in_file:

    state_names = []

    reader = csv.DictReader(in_file, delimiter=',', quotechar='"', lineterminator='\n')

    for p in reader:
        if p['SUMLEV'] == '040':
            state_names.append(p['NAME'])

    state_names = sorted(list(set(state_names)))

    @dataclass
    class State:
        name: str
        total_population: int = 0
        name_1: str = ''
        pop_1: int = 0
        name_2: str = ''
        pop_2: int = 0
        name_3: str = ''
        pop_3: int = 0
        name_4: str = ''
        pop_4: int = 0
        name_5: str = ''
        pop_5: int = 0
        gap: int = 0
        gap_relative = 0
        # populations: list = field(default_factory=list)
        c_o_v: float = 0  # Coefficient of variation.


    states_df = pd.DataFrame(
        columns=['name', 'total_population', 'name_1', 'pop_1', 'name_2', 'pop_2', 'name_3', 'pop_3', 'name_4', 'pop_4',
                 'name_5', 'pop_5', 'gap', 'gap_relative', 'c_o_v'])

    state_names = state_names[0:3]  # Truncates for debugging.

    for idx, sn in enumerate(state_names):

        s = State(name = sn)

        state_places = []

        in_file.seek(0)
        next(reader, None)

        for p in reader:
            if p['STNAME'] == sn and p['SUMLEV'] == SUMLEV:
                state_places.append(p)

        for sp in state_places:
            sp['population'] = int(sp['POPESTIMATE2019'])
            s.total_population += sp['population']

        state_places = sorted(state_places, key=lambda k: k['population'], reverse=True)

        s.name_1 = state_places[0]['NAME']
        s.pop_1 = int(state_places[0]['POPESTIMATE2019'])

        if len(state_places) > 1:  # Washington DC

            populations = []

            for sp in state_places:
                populations.append(int(sp['POPESTIMATE2019']))

            populations = sorted(populations)[-19:]
            populations_array = np.array(populations)
            s.c_o_v = variation(populations_array)

            s.name_2 = state_places[1]['NAME']
            s.pop_2 = int(state_places[1]['POPESTIMATE2019'])
            s.name_3 = state_places[2]['NAME']
            s.pop_3 = int(state_places[2]['POPESTIMATE2019'])
            s.name_4 = state_places[3]['NAME']
            s.pop_4 = int(state_places[3]['POPESTIMATE2019'])
            s.name_5 = state_places[4]['NAME']
            s.pop_5 = int(state_places[4]['POPESTIMATE2019'])

            s.gap = s.pop_1 - s.pop_2
            s.gap_relative = s.pop_1 / s.pop_2

            print('{}'.format(s.name))

        # row = [s.name, s.total_population, s.name_1, s.pop_1, s.name_2, s.pop_2,
        #        s.gap, s.gap_relative, s.c_o_v]
        # row_df = pd.DataFrame(row)

        states_df = states_df.append(
            {'name': s.name,
             'total_population': s.total_population,
             'name_1': s.name_1,
             'pop_1': s.pop_1,
             'name_2': s.name_2,
             'pop_2': s.pop_2,
             'name_3': s.name_3,
             'pop_3': s.pop_3,
             'name_4': s.name_4,
             'pop_4': s.pop_4,
             'name_5': s.name_5,
             'pop_5': s.pop_5,
             'gap': s.gap,
             'gap_relative': s.gap_relative,
             'c_o_v': s.c_o_v},
            ignore_index=True
        )

    states_df = states_df.sort_values(by='c_o_v', ascending=False)

    for idx, s in states_df.iterrows():
        print('{}: {} ({}), {} ({}). Gap {} ({}%)'
              .format(s['name'],
                      s['name_1'],
                      f'{s["pop_1"]:,}',
                      s['name_2'],
                      f'{s["pop_2"]:,}',
                      f'{s["gap"]:,}',
                      int(s["gap_relative"] * 100)))

        fig = plt.figure()
        ax = fig.add_axes([0,0,1,1])
        ax.set_title(s['name'])
        ax.set_xlabel('rank')
        populations = [s['pop_1'], s['pop_2'], s['pop_3'], s['pop_4'], s['pop_5']]
        # names = [s['name_1'], s['name_2'], s['name_3'], s['name_4'], s['name_5']]
        names = ['1', '2', '3', '4', '5']
        ax.bar(populations, names)
        plt.show()

    out_file_name = 'data\\lopsided_states_{}.csv'.format(SUMLEV)
    with open(out_file_name, 'w', newline='') as out_file:
        states_df.to_csv(out_file, index=False)

print('All done.')